<?php
class Palindrome
{
    public static function isPalindrome($word)
    {
    	$word = strtolower($word);
    	$chars = str_split($word);

    	$wordBack = "";
    	for($i=count($chars)-1; $i>=0; $i--){
    		$wordBack .= $chars[$i];
    	}
    	// echo $word." = ".$wordBack."<br>";
        return $word == $wordBack;
    }
}

echo Palindrome::isPalindrome('Deleveled');