<?php
class Pipeline
{
    public static function make_pipeline(...$funcs)
    {
        return function($arg) use ($funcs)
        {
        	$result = $arg;
          	foreach($funcs as $fun){
          		$result = $fun($result);
          	}
            return $result;
        };
    }
}

$fun = Pipeline::make_pipeline(function($x) { return $x * 3; }, function($x) { return $x + 1; },
                          function($x) { return $x / 2; });
echo $fun(3); # should print 5